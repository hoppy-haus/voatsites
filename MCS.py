# coding=utf-8
import urllib2
import json
from HTMLParser import HTMLParser
from flask import Flask, Markup, redirect, url_for
from BeautifulSoup import BeautifulSoup

app = Flask(__name__)
opener = urllib2.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]

html = HTMLParser()

linkcache = {}

def GetSiteMap(data):
    sitemap = opener.open('https://voat.co/'+data).read()
    site = BeautifulSoup(sitemap)
    stickied = site.findAll('div', {'class': 'usertext-body original may-blank-within'})[0]
    jsondata = json.loads(html.unescape(stickied.find('p').text))
    return jsondata

def ParsePage(id, sub):
    page = opener.open('https://voat.co/v/'+sub+'/'+str(id)).read()
    ppage = BeautifulSoup(page)
    data = ppage.findAll('div', {'class': 'usertext-body original may-blank-within'})[0]
    title = ppage.findAll('a', {'class': 'title may-blank '})[0].text
    cache = ''
    for bit in data.find('p').contents:
        cache = cache + str(bit).replace('"', '\\"')
    print html.unescape(cache)

    jsondata = json.loads(html.unescape(cache))
    return [title, jsondata]

def BuildPage(base, data):
    base = base.replace('++title', data[0])
    for tag, bit in data[1].iteritems():
        base = base.replace('++'+tag, bit)
    return base

@app.route('/s/<subsite>')
def addslash(subsite):
    return redirect('/s/'+subsite+'/')

@app.route("/s/<subsite>/")
def loadsite(subsite):
    site = opener.open('https://voat.co/v/'+subsite).read()
    parsed = BeautifulSoup(site)
    stickied = parsed.findAll('span', {'class' : 'promoted'})
    data = stickied[0].findNext('a').get('href')
    sitemap = GetSiteMap(data)
    content = ParsePage(sitemap['homepage'], subsite)
    page = parsed.findAll('style', {'id' : 'custom_css'})[0].getText().replace(u'∧', '<').replace(u'∨', '>')
    return BuildPage(page, content)

@app.route("/s/<subsite>/sitemap")
def loadsitemap(subsite):
    site = opener.open('https://voat.co/v/'+subsite).read()
    parsed = BeautifulSoup(site)
    stickied = parsed.findAll('span', {'class' : 'promoted'})
    data = stickied[0].findNext('a').get('href')
    sitemap = GetSiteMap(data)
    links = "<ul>"
    for k in sitemap:
        links += '<li><a href="' + str(sitemap[k]) + '">' + str(Markup.escape(k)) + '</a></li>'
    links += "</ul>"
    return Markup('<!DOCTYPE html><html><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title>' +
        '%s - sitemap</title>' +
        '<style type="text/css">body{margin:40px auto;max-width:650px;line-height:1.6;font-size:18px;color:#444;padding:010px}h1,h2,h3{line-height:1.2}</style>' +
        '</head><body><h1>%s - sitemap</h1>' + links + '</body></html>') % (subsite, subsite)

@app.route("/s/<subsite>/<page>")
def loadpage(subsite, page):
    site = opener.open('https://voat.co/v/'+subsite+'/'+page).read()
    parsed = BeautifulSoup(site)
    content = ParsePage(page, subsite)
    page = parsed.findAll('style', {'id' : 'custom_css'})[0].getText().replace(u'∧', '<').replace(u'∨', '>')
    return BuildPage(page, content)

if __name__ == "__main__":
    app.run()