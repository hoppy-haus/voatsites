# coding=utf-8
import sys

if sys.argv[1] == '-h':
    with open(sys.argv[2]) as f:
        data = unicode(f.read(), 'utf-8')
        data = data.replace(u'<', u'∧').replace(u'>', u'∨')
        print data